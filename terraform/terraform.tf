terraform {
  backend "s3" {
    region = "us-west-2"
    bucket = "your-bucket"
    key    = "nap.tfstate"
  }
}
