#################################################################
# Data sources to get VPC and subnets
##################################################################
data "aws_vpc" "vpc" {
  cidr_block = "10.4.0.0/16"
}

data "aws_subnet" "public_b" {
  vpc_id            = data.aws_vpc.vpc.id
  availability_zone = "us-west-2b"
  tags = {
    Name = "public-b-10-4-96-0"
  }
}

data "aws_subnet" "public_c" {
  vpc_id            = data.aws_vpc.vpc.id
  availability_zone = "us-west-2b"
  tags = {
    Name = "public-b-10-4-224-0"
  }
}

data "aws_security_group" "allow-mgmt-traffic-from-trusted-sources" {
  vpc_id = data.aws_vpc.vpc.id
  name   = "allow-mgmt-traffic-from-trusted-sources"
}

data "aws_security_group" "load_balancer" {
  vpc_id = data.aws_vpc.vpc.id
  name   = "app-load-balancer"
}

data "aws_lb_target_group" "nap" {
  name = "nap"
}




##################################################################
# NAP Resources
##################################################################
module "nap" {
  source = "terraform-aws-modules/ec2-instance/aws"
  providers = {
    aws = aws.us-west-2
  }
  version = "~> 2.0"

  instance_count = 2
  name           = "nap.us-west-2.int"
  ami            = "ami-045c0c07ba6b04fcc"
  instance_type  = "t2.medium"
  root_block_device = [
    {
      volume_type = "gp2"
      volume_size = 8
    }
  ]
  associate_public_ip_address = true
  key_name               = "aws-f5-fedorov"
  vpc_security_group_ids = [module.nap_sg.this_security_group_id, data.aws_security_group.allow-mgmt-traffic-from-trusted-sources.id]
  subnet_ids             = [data.aws_subnet.public_b.id, data.aws_subnet.public_c.id]
}

module "nap_sg" {
  source          = "terraform-aws-modules/security-group/aws"
  use_name_prefix = false

  name        = "nap"
  description = "nap"
  vpc_id      = data.aws_vpc.vpc.id

  ingress_with_source_security_group_id = [
    {
      from_port                = 80
      to_port                  = 80
      protocol                 = "TCP"
      description              = "HTTP from AWS app load balancer"
      source_security_group_id = data.aws_security_group.load_balancer.id
    },
  ]
}

resource "aws_lb_target_group_attachment" "nap" {
  count = length(module.nap.id)
  target_group_arn = data.aws_lb_target_group.nap.arn
  target_id        = module.nap[count.index].id
  port             = 80
}

resource "local_file" "hosts_cfg" {
  content = templatefile("hosts.tmpl",
    {
      nap_instances = module.nap.public_ip
    }
  )
  filename = "hosts.cfg"
}
